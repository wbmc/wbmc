# wbmc.
Lightweight Minecraft Server Core 
## goal.
Create Minecraft Server Core that can be easily run on RPi.
## word abbreviations used project-wide.
1. sc - server core.
2. mc - minecraft.
3. md - markdown.
4. da - different actions.
## to-do.
1. create sc barebones.
2. add craftbukkit plugin support.
3. plugins for wbmc.
4. optimise as much as it possible.
## contributing.
### commit naming scheme.
wbmc / (+/-/M)/da / (Filename)/2+ files.
### wiki naming scheme.
wbmc-wiki / (+/-/M) / (Wiki Page Name)
### variables naming.
every variable name should give sense.